﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using todo_aspnetmvc_ui.Data;
using todo_aspnetmvc_ui.Models;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Controllers
{
    public class ToDoListsController : Controller
    {
        private readonly ToDoContext _context;

        public ToDoListsController(ToDoContext context)
        {
            _context = context;
        }

        // GET: ToDoLists
        public async Task<IActionResult> Index()
        {
            var todayDate = DateTime.Now.ToString("yyyy-MM-dd");
            if (_context.ToDos.Any(x => x.DueDate == todayDate))
            {
                ViewBag.Name = "There are task with today's deadline! Please press Due Today button to check them out.";
            }

            return _context.ToDos != null ?
                        View(await _context.ToDos.ToListAsync()) :
                        Problem("Entity set 'ToDoContext.ToDos'  is null.");
        }

        // GET: ToDoLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.ToDos == null)
            {
                return NotFound();
            }

            var toDoList = await _context.ToDos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(new ToDoWithStatus()
            {
                Id = toDoList.Id,
                Status = _context.Statuses.First(x => x.Id == toDoList.StatusId).Name,
                Title = toDoList.Title,
                Description = toDoList.Description,
                DueDate = toDoList.DueDate,
                CreationDate = toDoList.CreationDate
            });
        }

        // GET: ToDoLists/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ToDoLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Status,Title,Description,DueDate,CreationDate")] ToDoWithStatus toDoWithStatus)
        {
            var toDo = new ToDoList()
            {
                Id = toDoWithStatus.Id,
                StatusId = int.Parse(toDoWithStatus.Status),
                Title = toDoWithStatus.Title,
                Description = toDoWithStatus.Description,
                DueDate = toDoWithStatus.DueDate,
                CreationDate = toDoWithStatus.CreationDate
            };

            if (ModelState.IsValid)
            {
                _context.Add(toDo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(toDo);
        }

        // GET: ToDoLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.ToDos == null)
            {
                return NotFound();
            }

            var toDoList = await _context.ToDos.FindAsync(id);
            if (toDoList == null)
            {
                return NotFound();
            }
            return View(new ToDoWithStatus()
            {
                Id = toDoList.Id,
                Status = _context.Statuses.First(x => x.Id == toDoList.StatusId).Name,
                Title = toDoList.Title,
                Description = toDoList.Description,
                DueDate = toDoList.DueDate,
                CreationDate = toDoList.CreationDate
            });
        }

        // POST: ToDoLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Status,Title,Description,DueDate,CreationDate")] ToDoWithStatus toDoWithStatus)
        {
            if (id != toDoWithStatus.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(new ToDoList()
                    {
                        Id = toDoWithStatus.Id,
                        StatusId = int.Parse(toDoWithStatus.Status),
                        Title = toDoWithStatus.Title,
                        Description = toDoWithStatus.Description,
                        DueDate = toDoWithStatus.DueDate,
                        CreationDate = toDoWithStatus.CreationDate
                    });
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoListExists(toDoWithStatus.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(toDoWithStatus);
        }

        // GET: ToDoLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.ToDos == null)
            {
                return NotFound();
            }

            var toDoList = await _context.ToDos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(new ToDoWithStatus()
            {
                Id = toDoList.Id,
                Status = _context.Statuses.First(x => x.Id == toDoList.StatusId).Name,
                Title = toDoList.Title,
                Description = toDoList.Description,
                DueDate = toDoList.DueDate,
                CreationDate = toDoList.CreationDate
            });
        }

        // POST: ToDoLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ToDos == null)
            {
                return Problem("Entity set 'ToDoContext.ToDos'  is null.");
            }
            var toDoList = await _context.ToDos.FindAsync(id);
            if (toDoList != null)
            {
                _context.ToDos.Remove(toDoList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> GetDueToday()
        {
            var todayDate = DateTime.Now.ToString("yyyy-MM-dd");
            var items = await _context.ToDos
                .Where(x => x.DueDate == todayDate)
                .Select(x => new ToDoWithStatus
                {
                    Id = x.Id,
                    Status = _context.Statuses.First(y => y.Id == x.StatusId).Name,
                    Title = x.Title,
                    Description = x.Description,
                    DueDate = x.DueDate,
                    CreationDate = x.CreationDate
                }).ToListAsync();

            return View("DueToday", items);
        }

        [HttpPost]
        public string CopyToClipboardTxt() 
        {
            var copiedTxt = "";
            copiedTxt += "InProgress\n\t";
            foreach (var item in _context.ToDos.Where(x => x.StatusId == 1)) 
            {
                copiedTxt += item.Title + "\t" + item.Description + "\t" + item.DueDate + "\t" + item.CreationDate + "\n\t";
            }
            copiedTxt += "\nUpcoming\n\t";
            foreach (var item in _context.ToDos.Where(x => x.StatusId == 2))
            {
                copiedTxt += item.Title + "\t" + item.Description + "\t" + item.DueDate + "\t" + item.CreationDate + "\n\t";
            }
            copiedTxt += "\nCompleted\n\t";
            foreach (var item in _context.ToDos.Where(x => x.StatusId == 3))
            {
                copiedTxt += item.Title + "\t" + item.Description + "\t" + item.DueDate + "\t" + item.CreationDate + "\n\t";
            }
            return copiedTxt;
        }

        private bool ToDoListExists(int id)
        {
            return (_context.ToDos?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
