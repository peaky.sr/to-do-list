﻿using Microsoft.EntityFrameworkCore;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Data
{
    public class ToDoContext : DbContext
    {
        public ToDoContext()
        {

        }

        public ToDoContext(DbContextOptions<ToDoContext> options)
            : base(options)
        {

        }

        public DbSet<ToDoList> ToDos { get; set; }
        public DbSet<Status> Statuses { get; set; }
    }
}
