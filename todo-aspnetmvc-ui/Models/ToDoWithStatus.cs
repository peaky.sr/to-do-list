﻿namespace todo_aspnetmvc_ui.Models
{
    public class ToDoWithStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DueDate { get; set; }
        public string CreationDate { get; set; }
    }
}
