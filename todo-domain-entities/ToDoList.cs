﻿namespace todo_domain_entities
{
    public class ToDoList
    {
        public int Id { get; set; }
        public int StatusId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DueDate { get; set; }
        public string CreationDate { get; set; }        
    }
}